# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require recipes-core/images/oniro-image-base.bb

SUMMARY = "Eddie blueprint image"
DESCRIPTION = "An Oniro device running Eddie to enable device interoperability"
LICENSE = "Apache-2.0"

IMAGE_INSTALL:append = "\ 
    eddie \
    "