# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require recipes-core/images/oniro-image-base.bb

SUMMARY = "Eddie blueprint gateway image"
DESCRIPTION = "An Oniro device running Eddie blueprint on top of Transparent Gateway"
LICENSE = "Apache-2.0"

IMAGE_INSTALL:append = "\ 
    eddie \
    packagegroup-thread-br \
    packagegroup-thread-client \
    networkmanager-softap-config \
    "