# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

# jsoncpp header files are placed in /usr/include/json but the libjsoncpp-dev 
# ubuntu package places them in /usr/include/jsoncpp/json.
# Here we make a copy to have the headers in both locations to be compatible with both. 
do_install:append() {
    mkdir ${D}/usr/include/jsoncpp
    cp -R ${D}/usr/include/json ${D}/usr/include/jsoncpp 
}

